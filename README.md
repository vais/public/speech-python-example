# Requirements
- python 3.6
- portaudio

# Install
```
pip install -r req.txt
```

# Run
```
python main.py
```
