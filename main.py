#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
#
import speech.v1.cloud_speech_pb2_grpc as cloud_speech_pb2_grpc
import speech.v1.cloud_speech_pb2 as cloud_speech_pb2
import grpc
import queue
import wave
import time
import sys
import signal, os
import capture



def connectivity_callback(c):
    pass

def state_callback(*args):
    pass


channel = grpc.insecure_channel("35.197.150.227:50051")
channel.subscribe(callback=connectivity_callback)
capture = capture.Capture({'sound': {'input_device': None}}, "/tmp/audio")
capture.setup(state_callback)

IS_STOP = False
def load_audio(fname):
    # Read and return a block of 640 frames (1280 bytes)
    fin = wave.open(fname)
    nframes = fin.getnframes()
    chunk_size = 640
    total_read = 0
    n_read = 0
    while total_read < nframes:
        n_read += 1
        nframe_left = nframes - total_read
        read_size = chunk_size if chunk_size < nframe_left else nframe_left

        data = fin.readframes(read_size)
        total_read += read_size
        yield data

def generate_message():
    audio_encode = cloud_speech_pb2.RecognitionConfig.LINEAR16
    speech_context = cloud_speech_pb2.SpeechContext(phrases=[])
    config = cloud_speech_pb2.RecognitionConfig(encoding=audio_encode, max_alternatives=1, speech_contexts=[speech_context])
    streaming_config = cloud_speech_pb2.StreamingRecognitionConfig(config=config, single_utterance=True, interim_results=True)

    request = cloud_speech_pb2.StreamingRecognizeRequest(streaming_config=streaming_config)
    yield request

    # audio_stream = capture.silence_listener()
    for audio in load_audio(sys.argv[1]):
    # for audio in audio_stream:
        request = cloud_speech_pb2.StreamingRecognizeRequest(audio_content=audio)
        yield request

def start_asr():
    global IS_STOP
    stub = cloud_speech_pb2_grpc.SpeechStub(channel)
    if len(sys.argv) < 3:
        print("API key is empty\nusage: python main.py audio.wav API_KEY")
        exit(1)
    metadata = [(b'api-key', sys.argv[2])]
    responses = stub.StreamingRecognize(generate_message(), metadata=metadata)
    for response in responses:
        if response.results:
            if response.results[0].alternatives:
                text = response.results[0].alternatives[0].transcript.strip()
                # text = text.replace("phờ lát", "flash")
                print(text, response.results[0].is_final)

def handler(signum, frame):
    global IS_STOP
    IS_STOP = True

if __name__ == "__main__":
    signal.signal(signal.SIGINT, handler)
    start_asr()
